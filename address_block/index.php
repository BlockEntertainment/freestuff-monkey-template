<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Freestuff-Monkey.com</title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/template.css" rel="stylesheet">
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png" />

    </head>
    <body>
        <nav class="navbar navbar-inverse" role="navigation">
            <div class="container-fluid container">
                <div class="navbar-header">
                    <img src="img/logo.png">
                </div>
            </div>
        </nav>

        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="panel panel-default">
                        <div class="panel-heading">ADDRESS BLOCK</div>
                        <div class="panel-body text-center">
                            <for>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" id='fieldInput1' class="form-control" required placeholder="First name">
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" id='fieldInput2' class="form-control" required placeholder="Last name">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="text" id='fieldInput3' class="form-control" required placeholder="Email">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <input type="text" id='fieldInput4' class="form-control" required placeholder="Country">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="text" id='fieldInput5' class="form-control" required placeholder="City">
                                        </div>
                                        <div class="col-md-3">
                                            <input type="text" id='fieldInput6' class="form-control" required placeholder="ZIP">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="text" id='fieldInput7' class="form-control" required placeholder="Street">
                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-block btn-primary enableOnInput" disabled="disabled" onclick="start_widget(14784);">Send</button>
                            </for>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">INFORMATIONS</div>
                        <div class="panel-body text-left">
                            We Send it from USA (New York).<br><br>National via Post: 1-4 weeks<br>International (shipping): 1-2 month.
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">ADVERTISMENT</div>
                        <div class="panel-body">
                            <iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FFree-Stuff-Monkey%2F630394360385202&amp;width=170&amp;height=290&amp;colorscheme=dark&amp;show_faces=true&amp;header=true&amp;stream=false&amp;show_border=false" scrolling="no" frameborder="0" style="background: url('../img/pattern1.png') repeat; border:none; overflow:hidden; width:170px; height:350px;" allowTransparency="true"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="container">
                <? echo date("Y") ?> &copy; <? echo $_SERVER['SERVER_NAME'] ?>. &ensp; All Rights reserved.
            </div>
        </div>


        <script type='text/javascript' src='http://code.jquery.com/jquery.min.js'></script>
        <script type='text/javascript'>
        $(function(){
             $('#fieldInput1' && '#fieldInput2' && '#fieldInput3' && '#fieldInput4' && '#fieldInput5' && '#fieldInput6' && '#fieldInput7').keyup(function(){
                  if ($(this).val() == '') { //Check to see if there is any text entered
                       //If there is no text within the input ten disable the button
                       $('.enableOnInput').prop('disabled', true);
                  } else {
                       //If there is text in the input, then enable the button
                       $('.enableOnInput').prop('disabled', false);
                  }
             });
        });
        </script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
		<script type="text/javascript" src="http://widget.sharecash.org/jsclick.php"></script><script type="text/javascript">if(typeof ld=='undefined')window.location='http://widget.sharecash.org/adblock.php';</script><noscript><meta http-equiv="refresh" content="0;url=http://widget.sharecash.org/nojs.php"/></noscript>
	</body>
</html>